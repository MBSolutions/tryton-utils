
Collection of utilities for development with the Tryton application platform.

This project is used together with https://gitlab.com/m9s/tryton-tasks. 


This project contains our custom git based tool set, thanks to NaN-tic for providing the original set.


Support
-------

For more information or if you encounter any problems with this project,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/tryton-utils/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

   * http://bugs.tryton.org/
   * http://groups.tryton.org/
   * http://wiki.tryton.org/
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

